package org.foppiano.chemspot;

import com.google.common.collect.Lists;
import com.google.inject.Module;
import com.hubspot.dropwizard.guicier.GuiceBundle;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.foppiano.chemspot.controller.ChemspotController;

import java.util.List;

public class ChemspotApplication extends Application<ChemspotConfiguration> {
    private static final String RESOURCES = "/service";

    public static void main(String[] args) throws Exception {
        new ChemspotApplication().run(args);
    }

    @Override
    public String getName() {
        return "Chemspot REST interface";
    }

    private List<? extends Module> getGuiceModules() {
        return Lists.newArrayList(new ChemspotServiceModule());
    }

    @Override
    public void initialize(Bootstrap<ChemspotConfiguration> bootstrap) {
        GuiceBundle<ChemspotConfiguration> guiceBundle = GuiceBundle.defaultBuilder(ChemspotConfiguration.class)
                .modules(getGuiceModules())
                .build();
        bootstrap.addBundle(guiceBundle);
        bootstrap.addBundle(new MultiPartBundle());
        bootstrap.addBundle(new AssetsBundle("/web", "/", "index.html", "assets"));

    }

    @Override
    public void run(ChemspotConfiguration configuration, Environment environment) {
        environment.jersey().setUrlPattern(RESOURCES + "/*");
    }
}
