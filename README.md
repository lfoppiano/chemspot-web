# Introduction

This project aims to provide a REST interface access to Chemspot. 
This is just a shell wrapping Chemspot [version 2.0](https://github.com/rockt/ChemSpot/releases/tag/v2.0).

Please bear in mind that this is still *Work in progress*

# Running The Application

To run the application run the following commands.

1. Clone the repository
> git clone git@bitbucket.org:lfoppiano/chemspot-web.git

2. Build the tool
> ./gradlew clean build

3. Run it!
> java -Xmx16G -jar build/libs/chemspot-web-0.0.1-SNAPSHOT-onejar.jar server data/config.yml


The service is reachable at the address `localhost:8080/service/chemspot/annotate`. 
Port and other options can be modified in `data/config.yml`

The output data is just a simple serialisation of the [`Mention` object from Chemspot](https://github.com/rockt/ChemSpot/blob/master/src/main/java/de/berlin/hu/chemspot/Mention.java).

Example: 

```
curl -X POST -F "text=The abilities of LHRH and a potent LHRH agonist ([D-Ser-(But),6,des-Gly-NH210]LHRH ethylamide) inhibit FSH responses by rat granulosa cells and Sertoli cells in vitro have been compared." http://localhost:8080/service/chemspot/annotate
```

response: 
```json
[
  {
    "start": 49,
    "end": 78,
    "text": "[D-Ser-(But),6,des-Gly-NH210]",
    "ids": [
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    ],
    "source": "eumed_tagger",
    "documentText": "The abilities of LHRH and a potent LHRH agonist ([D-Ser-(But),6,des-Gly-NH210]LHRH ethylamide) inhibit FSH responses by rat granulosa cells and Sertoli cells in vitro have been compared.",
    "type": "FORMULA",
    "cas": null,
    "pubc": null,
    "pubs": null,
    "inch": null,
    "drug": null,
    "hmbd": null,
    "kegg": null,
    "kegd": null,
    "mesh": null,
    "fda": null,
    "fdadate": null,
    "chid": null,
    "cheb": null
  },
  {
    "start": 83,
    "end": 93,
    "text": "ethylamide",
    "ids": [
      null,
      null,
      null,
      null,
      null,
      "InChI=1S/C2H6N/c1-2-3/h3H,2H2,1H3/q-1",
      null,
      null,
      null,
      null,
      null,
      null,
      null
    ],
    "source": "eumed_tagger",
    "documentText": "The abilities of LHRH and a potent LHRH agonist ([D-Ser-(But),6,des-Gly-NH210]LHRH ethylamide) inhibit FSH responses by rat granulosa cells and Sertoli cells in vitro have been compared.",
    "type": "SYSTEMATIC",
    "cas": null,
    "pubc": null,
    "pubs": null,
    "inch": "InChI=1S/C2H6N/c1-2-3/h3H,2H2,1H3/q-1",
    "drug": null,
    "hmbd": null,
    "kegg": null,
    "kegd": null,
    "mesh": null,
    "fda": null,
    "fdadate": null,
    "chid": null,
    "cheb": null
  },
  {
    "start": 103,
    "end": 106,
    "text": "FSH",
    "ids": [
      "009002680",
      null,
      "9002-68-0",
      null,
      null,
      null,
      "DB00066",
      null,
      null,
      null,
      "D005640",
      null,
      null
    ],
    "source": "dictionary",
    "documentText": "The abilities of LHRH and a potent LHRH agonist ([D-Ser-(But),6,des-Gly-NH210]LHRH ethylamide) inhibit FSH responses by rat granulosa cells and Sertoli cells in vitro have been compared.",
    "type": "SYSTEMATIC",
    "cas": "9002-68-0",
    "pubc": null,
    "pubs": null,
    "inch": null,
    "drug": "DB00066",
    "hmbd": null,
    "kegg": null,
    "kegd": null,
    "mesh": "D005640",
    "fda": null,
    "fdadate": null,
    "chid": "009002680",
    "cheb": null
  }
]
```
  