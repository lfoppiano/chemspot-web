package org.foppiano.chemspot;

import org.foppiano.chemspot.engine.ChemspotWrapper;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("health")
@Singleton
@Produces(APPLICATION_JSON)
public class HealthCheck extends com.codahale.metrics.health.HealthCheck {

    @Inject
    private ChemspotWrapper chemsportWrapper;

    @Inject
    public HealthCheck(ChemspotWrapper chemspotWrapper) {
        this.chemsportWrapper = chemspotWrapper;
    }

    @GET
    public Response alive() {
        return Response.ok().build();
    }

    @Override
    protected Result check() throws Exception {
        if (chemsportWrapper.check()) {
            return Result.healthy();
        } else {
            return Result.unhealthy("Cannot load the chemspot wrapper. Something went wrong.");
        }
    }
}
