package org.foppiano.chemspot.engine;

import de.berlin.hu.chemspot.ChemSpot;
import de.berlin.hu.chemspot.ChemSpotFactory;
import de.berlin.hu.chemspot.Mention;
import org.foppiano.chemspot.ChemspotConfiguration;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class ChemspotWrapper {

    private ChemSpot tagger;

    @Inject
    public ChemspotWrapper(ChemspotConfiguration configuration) {
        tagger = ChemSpotFactory.createChemSpot("data/dict.zip", "data/ids.zip", "data/multiclass.bin");
//        tagger = ChemSpotFactory.createChemSpot(null, null, null);
    }

    public List<Mention> annotate(String text) {
        List<Mention> tag = tagger.tag(text);
        return tag;
    }

    public boolean check() throws Exception {
        if (tagger != null) {
            return true;
        } else {
            return false;
        }
    }
}
