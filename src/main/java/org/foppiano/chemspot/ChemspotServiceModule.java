package org.foppiano.chemspot;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Binder;
import com.google.inject.Provides;
import com.hubspot.dropwizard.guicier.DropwizardAwareModule;
import org.foppiano.chemspot.controller.ChemspotController;
import org.foppiano.chemspot.engine.ChemspotWrapper;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;


public class ChemspotServiceModule extends DropwizardAwareModule<ChemspotConfiguration> {


    @Override
    public void configure(Binder binder) {
        // -- Generic modules --

        //Services
        binder.bind(ChemspotWrapper.class);
        binder.bind(HealthCheck.class);

        //REST
        binder.bind(ChemspotController.class);
    }

    @Provides
    protected ObjectMapper getObjectMapper() {
        return getEnvironment().getObjectMapper();
    }

    @Provides
    protected MetricRegistry provideMetricRegistry() {
        return getMetricRegistry();
    }

    //for unit tests
    protected MetricRegistry getMetricRegistry() {
        return getEnvironment().metrics();
    }

    @Provides
    Client provideClient() {
        return ClientBuilder.newClient();
    }

}