buildscript {
    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
    }
    dependencies {
        classpath 'org.kt3k.gradle.plugin:coveralls-gradle-plugin:2.4.0'
        classpath 'com.jfrog.bintray.gradle:gradle-bintray-plugin:1.7.3'
        classpath 'com.github.jengelman.gradle.plugins:shadow:2.0.4'
    }
}



apply plugin: 'java'
apply plugin: 'eclipse'
apply plugin: 'idea'
apply plugin: 'com.github.johnrengelman.shadow'
apply plugin: 'application'

repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
    maven { url "https://dl.bintray.com/rookies/maven" }
}


group = 'org.foppiano'
version = '0.0.1-SNAPSHOT'

description = "Web interface for Chemspot"

sourceCompatibility = 1.8
targetCompatibility = 1.8

tasks.withType(JavaCompile) {
    options.encoding = 'UTF-8'
}

sourceSets.main.resources {
    srcDirs = ["src/main/resources", "data/config"];
}


ext {
    // treating them separately, these jars will be flattened into grobid-core.jar on installing,
    // to avoid missing dependencies from the projects that include grobid-core (see 'jar' taskin grobid-core)
    localLibs = []
}

dependencies {
    compile fileTree(dir: new File(rootProject.rootDir, 'localLibs'), include: localLibs)
    
    // Logging
    compile('org.slf4j:slf4j-api:1.7.25')

    //Tests
    testCompile group: 'junit', name: 'junit', version: '4.12'
    testCompile group: 'org.easymock', name: 'easymock', version: '3.4'
    testCompile group: 'org.hamcrest', name: 'hamcrest-all', version: '1.3'

    //Dropwizard
    compile "io.dropwizard:dropwizard-core:1.3.6"
    compile "io.dropwizard:dropwizard-assets:1.3.6"
    compile "com.hubspot.dropwizard:dropwizard-guicier:1.3.5.0"
    compile "io.dropwizard:dropwizard-testing:1.3.6"
    compile "io.dropwizard:dropwizard-forms:1.3.6"
    compile "io.dropwizard:dropwizard-client:1.3.6"
    compile "io.dropwizard:dropwizard-auth:1.3.6"
    compile "io.dropwizard.metrics:metrics-core:4.0.0"
    compile "io.dropwizard.metrics:metrics-servlets:4.0.0"
}

tasks.run.workingDir = rootProject.rootDir

/*** Packaging and distribution ***/

import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

task mainJar(type: ShadowJar, group: 'onejar') {
    zip64 true
    from sourceSets.main.output
    mergeServiceFiles()
    from {
        project.configurations.compile.collect {
            it.isDirectory() ? [] : localLibs.contains(it.getName()) ? zipTree(it) : []
        }
    }
}

shadowJar {
    mainClassName = 'org.foppiano.chemspot.ChemspotApplication'
    classifier = 'onejar'
    mergeServiceFiles()
    zip64 true
    manifest {
        attributes 'Main-Class': 'org.foppiano.chemspot.ChemspotApplication'
    }
//    from sourceSets.main.output
//    configurations = [project.configurations.compile, project.configurations.runtime]
}

jar {
    dependsOn mainJar
    enabled false
}

artifacts {
//    archives shadowJar
    archives mainJar
}

distZip.classifier = 'application'
distTar.classifier = 'application'
