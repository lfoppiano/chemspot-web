package org.foppiano.chemspot.controller;

import com.codahale.metrics.annotation.Timed;
import de.berlin.hu.chemspot.Mention;
import org.foppiano.chemspot.engine.ChemspotWrapper;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Singleton
@Path("/chemspot/annotate")
@Timed
public class ChemspotController {

    private final ChemspotWrapper chemspotWrapper;

    @Inject
    public ChemspotController(ChemspotWrapper chemspotWrapper) {
        this.chemspotWrapper = chemspotWrapper;
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Mention> annotateGet(String text) {
        return chemspotWrapper.annotate(text);
    }


    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Mention> annotatePost(@FormDataParam("text") String text) {
        return chemspotWrapper.annotate(text);
    }
}
